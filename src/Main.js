import React, { Component } from 'react';

class Main extends Component {
    render() {
        return (
            <div className="single-content container" style={{textAlign: (this.props.direction === 'rtl') ? 'right' : 'left'}}>
                <div>
                    <div style={{[(this.props.direction === 'rtl') ? 'right' : 'left']: '30%'}}>{this.props.message.main}</div>
                </div>
            </div>
        )
    }
}
export default Main;