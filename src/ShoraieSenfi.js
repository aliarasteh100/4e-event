import React, { Component } from 'react';

class ShoraieSenfi extends Component {
    componentWillMount() {
        document.body.className = 'single-content-background-image';
        this.props.changeLanguageVisibility(false);
    }
    render() {
        return (
            <div className="single-content container" style={{textAlign: (this.props.direction === 'rtl') ? 'right' : 'left'}}>
                <div>
                    <div style={{[(this.props.direction === 'rtl') ? 'right' : 'left']: '30%'}}>{this.props.message.shoraieSenfi}</div>
                </div>
            </div>
        )
    }
}
export default ShoraieSenfi;