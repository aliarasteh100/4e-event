import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import { FaLanguage } from "react-icons/fa";
import { IoMdLogOut } from "react-icons/io";
import axios from "axios";
import API from "./API/API";
import Auth from "./Auth/Auth";

class Header extends Component {

    logout = () => {
        axios({
            url: `${API.baseURL}/logout/`,
            method: "post",
            data: {
                lang: (this.props.direction === 'rtl') ? 'fa' : 'en',
                client_id: Auth.client_id,
                client_secret: Auth.client_secret,
                grant_type: "password",
            },
            headers: {
                "content-type": "application/json",
                "Access-Control-Allow-Origin": "*",
                "Authorization": `${localStorage.getItem("token_type")} ${localStorage.getItem("access_token")}`
            },
            crossDomain: true,
        }).then(() => {
            localStorage.removeItem('refresh_token');
            localStorage.removeItem('token_type');
            localStorage.removeItem('access_token');
            localStorage.removeItem('expires_in');
            this.props.isAuthenticatedFunction();
        }).catch(err => {
            console.log(err.message)
        });
    };
    render() {
        return (
            <div className="top-header container" style={{direction: (this.props.direction === 'rtl') ? 'rtl' : 'ltr', textAlign: (this.props.direction === 'rtl') ? 'right' : 'left'}}>
                <div className="row">
                    <div className="col container-fluid">
                        <div className="links-container row">
                            {/*<div className="col links-container-cols" style={{[(this.props.direction === 'rtl') ? 'paddingRight' : 'paddingLeft'] : '15px'}}>*/}
                                {/*<Link to="/">{this.props.message.home}</Link>*/}
                            {/*</div>*/}
                            <div className="col links-container-cols">
                                <Link to={(this.props.isAuthenticated) ? '/game' : '/login'}>{(this.props.isAuthenticated) ? this.props.message.game : this.props.message.login}</Link>
                            </div>
                            <div className="col">
                            </div>
                        </div>
                    </div>
                    <div className="col-1 col-sm-1 col-md-3 col-lg-6 col-xl-6">
                    </div>
                    <div className="icon-container col container-fluid">
                        <div className="row no-gutters">
                            <div className="col">
                            </div>
                            <div className="col">
                            </div>
                            <div className={`${(this.props.isAuthenticated) ? 'change-language-icon' : ''} col`}>
                                <div>
                                    {(this.props.isAuthenticated) ? <FaLanguage onClick={() => this.props.changeLanguage('change')} className={(this.props.changeLanguageVisibility) ? '' : 'd-none'}/> : ''}
                                </div>
                            </div>
                            <div className={`${(this.props.isAuthenticated) ? 'logout-icon' : 'change-language-icon'} col`}>
                                <div>
                                    {(this.props.isAuthenticated) ? <IoMdLogOut onClick={this.logout} className={(this.props.isAuthenticated) ? '' : 'd-none'} style={{transform: (this.props.direction === 'rtl') ? 'rotate(180deg)' : 'rotate(0deg)'}}/> : <FaLanguage onClick={() => this.props.changeLanguage('change')} className={(this.props.changeLanguageVisibility) ? '' : 'd-none'}/>}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default Header;