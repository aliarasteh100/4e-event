import React, { Component } from 'react';
import { Link } from "react-router-dom";
import Logo from "./assets/images/Logo/Logo With Gradient Black to Yellow.png"

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            headers: ['introduction','prize','schedule','FAQ','contactUs'],
            focusedHeader: '',
            transparentHeader: true
        }
    }
    changeSection = (header,event) => {
        event.preventDefault();
        window.scrollTo({
            top: this.refs[header].offsetTop - 105,
            [(this.props.direction === 'rtl') ? 'right' : 'left']: 0,
            behavior: 'smooth'
        });
        setTimeout(() => {
            this.setState({
                focusedHeader: header
            });
        }, 1500);
    };
    componentWillMount() {
        document.body.className = 'blue-background-image';
        this.props.changeLanguageVisibility(true);
    }
    componentDidMount() {
        window.addEventListener('scroll', this.handleScroll);
    }
    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScroll);
    }
    handleScroll = () => {
        let flag = false;
        this.state.headers.forEach((header) => {
            if (window.scrollY >= (this.refs[header].offsetTop - 110)) {
                this.setState({
                    focusedHeader: header
                });
                flag = true;
            }
        });
        if (!flag) {
            this.setState({
                focusedHeader: ''
            });
        }
        (this.refs.header.offsetTop >= 70) ? this.setState({transparentHeader: false}) : this.setState({transparentHeader: true});
    };
    render() {
        return (
            <div className="home">
                <div className="home-header container-fluid" ref="header" style={{direction: (this.props.direction === 'rtl') ? 'rtl' : 'ltr', textAlign: (this.props.direction === 'rtl') ? 'right' : 'left', backgroundColor: (this.state.transparentHeader) ? 'transparent' : '#000022'}}>
                    <div className="container">
                        <div className="links-container row">
                            {this.state.headers.map((header,i) => (
                                    <div key={i} className="links-container-cols col" style={{borderBottomStyle: (header === this.state.focusedHeader) ? 'solid' : 'none'}}>
                                        <Link to={`/#${header}`} onClick={(event) => this.changeSection(header,event)}>{this.props.message[header]}</Link>
                                    </div>
                                )
                            )}
                        </div>
                    </div>
                </div>
                <div className="logos container">
                    <div className="row">
                        <div className="col">
                            <img src={Logo} alt="4E Logo"/>
                        </div>
                    </div>
                </div>
                <div className="section container" style={{textAlign: (this.props.direction === 'rtl') ? 'right' : 'left'}}>
                    <div ref="introduction">
                        <div style={{[(this.props.direction === 'rtl') ? 'right' : 'left']: '30%'}}>{this.props.message.introduction}</div>
                    </div>
                    <hr/>
                    <div ref="prize">
                        <div style={{[(this.props.direction === 'rtl') ? 'right' : 'left']: '30%'}}>{this.props.message.prize}</div>
                    </div>
                    <hr/>
                    <div ref="schedule">
                        <div style={{[(this.props.direction === 'rtl') ? 'right' : 'left']: '30%'}}>{this.props.message.schedule}</div>
                    </div>
                    <hr/>
                    <div ref="FAQ">
                        <div style={{[(this.props.direction === 'rtl') ? 'right' : 'left']: '30%'}}>{this.props.message.FAQ}</div>
                    </div>
                    <hr/>
                    <div ref="contactUs">
                        <div style={{[(this.props.direction === 'rtl') ? 'right' : 'left']: '30%'}}>{this.props.message.contactUs}</div>
                    </div>
                </div>
            </div>
        )
    }
}
export default Home;