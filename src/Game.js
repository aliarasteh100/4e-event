import React, { Component } from 'react';
import { Link } from "react-router-dom";
import Map from "./Map"
import GuestMap from "./GuestMap"
import Exchange from "./Exchange"
import Ranking from "./Ranking"
import Messages from "./Messages"

class Game extends Component {
    constructor(props) {
        super(props);
        this.state = {
            headers: ['map','exchange','ranking','messages'],
            guestHeaders: ['map','ranking'],
            focusedHeader: '',
            transparentHeader: true
        }
    }
    changeSection = (header,event) => {
        event.preventDefault();
        window.scrollTo({
            top: this.refs[header].offsetTop - 105,
            [(this.props.direction === 'rtl') ? 'right' : 'left']: 0,
            behavior: 'smooth'
        });
        setTimeout(() => {
            this.setState({
                focusedHeader: header
            });
        }, 1500);
    };
    componentWillMount() {
        document.body.className = 'green-background-image';
        this.props.changeLanguageVisibility(false);
        this.props.changeLanguage('fa');
    }
    componentDidMount() {
        window.addEventListener('scroll', (localStorage.getItem('is_guest') === 'true') ? this.guestHandleScroll : this.handleScroll);
    }
    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScroll);
    }
    handleScroll = () => {
        let flag = false;
        this.state.headers.forEach((header) => {
            if (window.scrollY >= (this.refs[header].offsetTop - 110)) {
                this.setState({
                    focusedHeader: header
                });
                flag = true;
            }
        });
        if (!flag) {
            this.setState({
                focusedHeader: ''
            });
        }
        (this.refs.header.offsetTop >= 70) ? this.setState({transparentHeader: false}) : this.setState({transparentHeader: true});
    };
    guestHandleScroll = () => {
        let flag = false;
        this.state.guestHeaders.forEach((header) => {
            if (window.scrollY >= (this.refs[header].offsetTop - 110)) {
                this.setState({
                    focusedHeader: header
                });
                flag = true;
            }
        });
        if (!flag) {
            this.setState({
                focusedHeader: ''
            });
        }
        (this.refs.header.offsetTop >= 70) ? this.setState({transparentHeader: false}) : this.setState({transparentHeader: true});
    };
    render() {
        return (
            <div className="game">
                {
                    (localStorage.getItem('is_guest') === 'true') ? (
                        <div>
                           <div className="game-header container-fluid" ref="header" style={{direction: (this.props.direction === 'rtl') ? 'rtl' : 'ltr', textAlign: (this.props.direction === 'rtl') ? 'right' : 'left', backgroundColor: (this.state.transparentHeader) ? 'transparent' : '#052200'}}>
                               <div className="container">
                                   <div className="links-container row">
                                       {this.state.guestHeaders.map((header,i) => (
                                               <div key={i} className="links-container-cols col" style={{borderBottomStyle: (header === this.state.focusedHeader) ? 'solid' : 'none'}}>
                                                   <Link to={`/game/#${header}`} onClick={(event) => this.changeSection(header,event)}>{this.props.message[header]}</Link>
                                               </div>
                                           )
                                       )}
                                   </div>
                               </div>
                           </div>
                           <div className="section container" style={{textAlign: (this.props.direction === 'rtl') ? 'right' : 'left'}}>
                               {/*<div ref="characteristics">*/}
                               {/*<Characteristics direction={this.props.direction} message={this.props.message}/>*/}
                               {/*</div>*/}
                               {/*<hr/>*/}
                               <div ref="map">
                                   <GuestMap direction={this.props.direction} message={this.props.message} map={this.props.map}/>
                               </div>
                               <hr/>
                               <div ref="ranking">
                                   <Ranking direction={this.props.direction} message={this.props.message} ranks={this.props.ranks}/>
                               </div>
                           </div>
                       </div>
                    ) : (
                        <div>
                            <div className="game-header container-fluid" ref="header" style={{direction: (this.props.direction === 'rtl') ? 'rtl' : 'ltr', textAlign: (this.props.direction === 'rtl') ? 'right' : 'left', backgroundColor: (this.state.transparentHeader) ? 'transparent' : '#052200'}}>
                                <div className="container">
                                    <div className="links-container row">
                                        {this.state.headers.map((header,i) => (
                                                <div key={i} className="links-container-cols col" style={{borderBottomStyle: (header === this.state.focusedHeader) ? 'solid' : 'none'}}>
                                                    <Link to={`/game/#${header}`} onClick={(event) => this.changeSection(header,event)}>{this.props.message[header]}</Link>
                                                </div>
                                            )
                                        )}
                                    </div>
                                </div>
                            </div>
                            <div className="section container" style={{textAlign: (this.props.direction === 'rtl') ? 'right' : 'left'}}>
                                {/*<div ref="characteristics">*/}
                                {/*<Characteristics direction={this.props.direction} message={this.props.message}/>*/}
                                {/*</div>*/}
                                {/*<hr/>*/}
                                <div ref="map">
                                    <Map direction={this.props.direction} message={this.props.message} map={this.props.map}/>
                                </div>
                                <hr/>
                                <div ref="exchange">
                                    <Exchange direction={this.props.direction} message={this.props.message} pointToMoney={this.props.pointToMoney} moneyToPoint={this.props.moneyToPoint}/>
                                </div>
                                <hr/>
                                <div ref="ranking">
                                    <Ranking direction={this.props.direction} message={this.props.message} ranks={this.props.ranks}/>
                                </div>
                                <hr/>
                                <div ref="messages">
                                    <Messages direction={this.props.direction} message={this.props.message} messages={this.props.messages} messagesFunction={this.props.messagesFunction}/>
                                </div>
                            </div>
                        </div>
                    )
                }

            </div>
        )
    }
}
export default Game;