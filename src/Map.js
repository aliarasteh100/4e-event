import React, { Component } from 'react';
import axios from "axios";
import API from "./API/API";
import Auth from "./Auth/Auth";

class Map extends Component {
    constructor(props) {
        super(props);
        this.state = {
            chosenTask: {},
            flag: true,
            errorMessageVisibility: false
        }
    }
    changeChosenTask = (data,event) => {
        event.stopPropagation();
        this.setState({
            chosenTask: data
        });
        this.refs.errorMessage.innerHTML = '';
    };
    noChosenTask = () => {
        this.setState({
            chosenTask: ""
        });
        this.refs.errorMessage.innerHTML = '';
    };
    buyNode = (event) => {
        event.stopPropagation();
        axios({
            url: `${API.baseURL}/buy-node/`,
            method: "post",
            data: {
                lang: (this.props.direction === 'rtl') ? 'fa' : 'en',
                client_id: Auth.client_id,
                client_secret: Auth.client_secret,
                grant_type: "password",
                row: this.state.chosenTask.row,
                col1: this.state.chosenTask.col1,
                col2: this.state.chosenTask.col2
            },
            headers: {
                "content-type": "application/json",
                "Access-Control-Allow-Origin": "*",
                "Authorization": `${localStorage.getItem("token_type")} ${localStorage.getItem("access_token")}`
            },
            crossDomain: true,
        }).then(() => {
            this.displayError(this.props.message.successfulBuyNode);
        }).catch(err => {
            if (err.response.data) {
                if (err.response.data.error) {
                    this.displayError(err.response.data.error)
                }
            } else {
                this.displayError(err.message)
            }
            this.setState({
                loading: false
            });
        });
    };
    displayError = (error) => {
        this.refs.errorMessage.innerHTML = error;
        this.setState({
            errorMessageVisibility: true
        })
    };
    render() {
        return (
            <div className="map-page container" style={{textAlign: (this.props.direction === 'rtl') ? 'right' : 'left'}}>
                <div className="map">
                    <div className="header" style={{[(this.props.direction === 'rtl') ? 'right' : 'left']: '30%'}}>{this.props.message.map}</div>
                    <div className="row" onClick={this.noChosenTask}>
                        <div className="map-column col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                            <div className="map-container container">
                                {this.props.map.map((row) => (
                                        <div className="row" key={row.id}>
                                            {row.rowElements.map((primaryCol) => (
                                                    <div className="column-style col container-fluid" key={primaryCol.id}>
                                                        {primaryCol.colElements.map((secondaryCol) =>
                                                            <div className="col" key={secondaryCol.id}>
                                                                {(!secondaryCol.must_show) ? (
                                                                    <div>
                                                                    </div>
                                                                ) : (
                                                                    (!secondaryCol.buy_obj && (secondaryCol.states.length === 0)) ? (
                                                                        <div onClick={(event) => this.changeChosenTask(secondaryCol,event)} className='lock-background'>
                                                                        </div>
                                                                    ) : (
                                                                        (secondaryCol.buy_obj) ? (
                                                                            <div onClick={(event) => this.changeChosenTask(secondaryCol,event)} className='saleable-background'>
                                                                                <p>{secondaryCol.level}</p>
                                                                            </div>
                                                                        ) : (
                                                                            (!secondaryCol.states[0].is_done) ? (
                                                                                <div onClick={(event) => this.changeChosenTask(secondaryCol,event)} className='pending-background'>
                                                                                    <p>{secondaryCol.level}</p>
                                                                                </div>
                                                                            ) : (
                                                                                <div onClick={(event) => this.changeChosenTask(secondaryCol,event)} className='done-background'>
                                                                                    <p>{secondaryCol.level}</p>
                                                                                </div>
                                                                            )
                                                                        )
                                                                    )
                                                                )}
                                                            </div>
                                                        )}
                                                    </div>
                                                )
                                            )}
                                        </div>
                                    )
                                )}
                            </div>
                            <p>برای دیدن اطلاعات مربوط به یک تسک، در نقشه بالا روی خانه مربوط به آن تسک کلیک کنید</p>
                        </div>
                        <div className="task-detail col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 container">
                            {(Object.keys(this.state.chosenTask).length === 0) ? (
                                <div className="no-task-chosen">
                                    <p>تسکی انتخاب نشده است</p>
                                </div>
                             ) : (
                                (!this.state.chosenTask.buy_obj && (this.state.chosenTask.states.length === 0)) ? (
                                    <div className="no-task-chosen">
                                        <p>هنوز به تسک انتخاب شده دسترسی ندارید</p>
                                    </div>
                                ) : (
                                    (this.state.chosenTask.buy_obj) ? (
                                        <div className="chosen-task">
                                            <p>سطح:&nbsp;{this.state.chosenTask.level}</p>
                                            <p>نام:&nbsp;{this.state.chosenTask.buy_obj.task.name}</p>
                                            <p>هزینه:&nbsp;{this.state.chosenTask.buy_obj.task.cost}</p>
                                            <button onClick={this.buyNode}>خرید</button>
                                        </div>
                                    ) : (
                                        (!this.state.chosenTask.states[0].is_done) ? (
                                            <div className="chosen-task">
                                                <p>سطح:&nbsp;{this.state.chosenTask.level}</p>
                                                <p>نام:&nbsp;{this.state.chosenTask.states[0].task.name}</p>
                                                <p>فایل توضیحات:</p>
                                                <a href={this.state.chosenTask.states[0].task.file} target="_blank" rel="noopener noreferrer" onClick={(event) => event.stopPropagation()} download>برای دانلود فایل کلیک کنید.</a>
                                                <p>محل انجام:&nbsp;{this.state.chosenTask.states[0].task.place}</p>
                                                <p>هزینه:&nbsp;{this.state.chosenTask.states[0].task.cost}</p>
                                                <p>امتیاز:&nbsp;{this.state.chosenTask.states[0].task.point}</p>
                                                <p>وضعیت:&nbsp;انجام نشده است</p>
                                            </div>
                                        ) : (
                                            <div className="chosen-task">
                                                <p>سطح:&nbsp;{this.state.chosenTask.level}</p>
                                                <p>نام:&nbsp;{this.state.chosenTask.states[0].task.name}</p>
                                                <p>فایل توضیحات:</p>
                                                <a href={this.state.chosenTask.states[0].task.file} target="_blank" rel="noopener noreferrer" onClick={(event) => event.stopPropagation()} download>برای دانلود فایل کلیک کنید.</a>
                                                <p>محل انجام:&nbsp;{this.state.chosenTask.states[0].task.place}</p>
                                                <p>هزینه:&nbsp;{this.state.chosenTask.states[0].task.cost}</p>
                                                <p>امتیاز:&nbsp;{this.state.chosenTask.states[0].task.point}</p>
                                                <p>وضعیت:&nbsp;انجام شده است</p>
                                            </div>
                                        )
                                    )
                                )

                            )
                            }
                            <div className={`error-message ${(this.state.errorMessageVisibility) ? '' : 'd-none'}`} style={{textAlign: ((this.props.direction === 'rtl') ? 'right' : 'left'), marginTop: '20px'}}>
                                <p ref='errorMessage'>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default Map;
