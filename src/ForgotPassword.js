import React, { Component } from 'react';
import { FaEnvelope } from "react-icons/fa";
import { FaUser } from "react-icons/fa";
import { FaLongArrowAltLeft } from "react-icons/fa"
import { FaLongArrowAltRight } from "react-icons/fa"
import axios from "axios";
import API from "./API/API";
import Auth from "./Auth/Auth";

class ForgotPassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
            errorMessageVisibility: false,
            loading: false
        }
    }
    componentWillMount() {
        document.body.className = 'cyan-background-image';
        this.props.changeLanguageVisibility(true);
    }
    changePassword = () => {
        let email = this.refs.email.value;
        this.props.auth.changePassword(email, this.displayError);
    };
    forgotPassword = () => {
        let email = this.refs.email.value;
        this.setState({
            loading: true
        });
        axios({
            url: `${API.baseURL}/forgot-password/`,
            method: "post",
            data: {
                lang: (this.props.direction === 'rtl') ? 'fa' : 'en',
                grant_type: "password",
                client_id: Auth.client_id,
                client_secret: Auth.client_secret,
                email: email
            },
            headers: {
                "content-type": "application/json",
                "Access-Control-Allow-Origin": "*"
            },
            crossDomain: true,
        }).then(() => {
            this.displayError(this.props.message.successfulSendOfForgotPasswordEmail);
            this.refs.email.value = '';
            this.setState({
                loading: false
            });
        }).catch(err => {
            if (err.response.data) {
                if (err.response.data.error) {
                    console.log(err.response.data);
                    this.displayError(err.response.data.error)
                }
            } else {
                this.displayError(err.message)
            }
            this.setState({
                loading: false
            });
        });
    };
    displayError = (error) => {
        this.refs.errorMessage.innerHTML = error;
        this.setState({
            errorMessageVisibility: true
        })
    };
    render() {
        return (
            <div className="login container">
                <div className="row">
                    <div className="col">
                    </div>
                    <div className="user col-10 col-sm-10 col-md-8 col-lg-6 col-xl-6">
                        <div className="row no-gutters">
                            <div className="col">
                            </div>
                            <div className="col-4 col-sm-4 col-md-3 col-lg-2 col-xl-2">
                                <FaUser className="user-icon"/>
                            </div>
                            <div className="col">
                            </div>
                        </div>
                        <div className="login-form">
                            <div className={`error-message ${(this.state.errorMessageVisibility) ? '' : 'd-none'}`} style={{textAlign: ((this.props.direction === 'rtl') ? 'right' : 'left'), marginTop: '20px'}}>
                                <p ref='errorMessage'>
                                </p>
                            </div>
                            <label htmlFor='Email' style={{textAlign: ((this.props.direction === 'rtl') ? 'right' : 'left'), [(this.props.direction === 'rtl') ? 'marginRight' : 'marginLeft']: '10%'}}><FaEnvelope/>&nbsp;&nbsp;&nbsp;{this.props.message.emailWithColon}</label>
                            <input id='Email' type='text' maxLength="50" placeholder={this.props.message.email} ref='email' style={{textAlign: (this.props.direction === 'rtl') ? 'right' : 'left', [(this.props.direction === 'rtl') ? 'marginRight' : 'marginLeft']: '10%', [(this.props.direction === 'rtl') ? 'paddingLeft' : 'paddingRight']: '13.5%'}} />
                            <div>
                                <button onClick={this.forgotPassword}>{this.props.message.sendEmail}&nbsp;&nbsp;&nbsp;{(this.props.direction === 'rtl') ? <FaLongArrowAltLeft style={{animation: (this.state.loading) ? '0.75s loading ease-in-out infinite' : 'none' }} className="login-icon"/> : <FaLongArrowAltRight style={{animation: (this.state.loading) ? '0.75s loading ease-in-out infinite' : 'none' }} className="login-icon"/>}</button>
                            </div>
                        </div>
                    </div>
                    <div className="col">
                    </div>
                </div>
            </div>
        )
    }
}
export default ForgotPassword;






