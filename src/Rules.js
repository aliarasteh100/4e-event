import React, { Component } from 'react';

class Rules extends Component {
    componentWillMount() {
        document.body.className = 'single-content-background-image';
        this.props.changeLanguageVisibility(true);
    }
    render() {
        return (
            <div className="single-content container" style={{textAlign: (this.props.direction === 'rtl') ? 'right' : 'left'}}>
                <div>
                    <div style={{[(this.props.direction === 'rtl') ? 'right' : 'left']: '30%'}}>{this.props.message.rules}</div>
                </div>
            </div>
        )
    }
}
export default Rules;