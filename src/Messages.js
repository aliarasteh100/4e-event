import React, { Component } from 'react';
import axios from "axios";
import API from "./API/API";
import Auth from "./Auth/Auth";

class Messages extends Component {
    checkedMessages = (message) => {
        axios({
            url: `${API.baseURL}/update-message/`,
            method: "post",
            data: {
                lang: (this.props.direction === 'rtl') ? 'fa' : 'en',
                client_id: Auth.client_id,
                client_secret: Auth.client_secret,
                grant_type: "password",
                id: message.id,
            },
            headers: {
                "content-type": "application/json",
                "Access-Control-Allow-Origin": "*",
                "Authorization": `${localStorage.getItem("token_type")} ${localStorage.getItem("access_token")}`
            },
            crossDomain: true,
        }).then(() => {
            this.props.messagesFunction();
        }).catch(err => {
            console.log(err.message);
        });
    };
    render() {
        return (
            <div className="single-content-page container" style={{textAlign: (this.props.direction === 'rtl') ? 'right' : 'left'}}>
                <div>
                    <div className="header" style={{[(this.props.direction === 'rtl') ? 'right' : 'left']: '30%'}}>{this.props.message.messages}</div>
                    {
                        this.props.messages.map((message) => (<div className='message' key={message.id} ref={message.id} onClick={() => this.checkedMessages(message)}>{(message.is_seen) ? '' : (<p>جدید</p>)}{message.fa_message}</div>))
                    }
                </div>
            </div>
        )
    }
}
export default Messages;