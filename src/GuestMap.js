import React, { Component } from 'react';

class GuestMap extends Component {
    render() {
        return (
            <div className="map-page container" style={{textAlign: (this.props.direction === 'rtl') ? 'right' : 'left'}}>
                <div className="map">
                    <div className="header" style={{[(this.props.direction === 'rtl') ? 'right' : 'left']: '30%'}}>{this.props.message.map}</div>
                    <div className="row" onClick={this.noChosenTask}>
                        <div className="col">
                        </div>
                        <div className="map-column col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                            <div className="map-container container">
                                {this.props.map.map((row) => (
                                        <div className="row" key={row.id}>
                                            {row.rowElements.map((primaryCol) => (
                                                    <div className="column-style col container-fluid" key={primaryCol.id}>
                                                        {primaryCol.colElements.map((secondaryCol) =>
                                                            <div className="col" key={secondaryCol.id}>
                                                                {(!secondaryCol.must_show) ? (
                                                                        <div>
                                                                        </div>
                                                                    ) : (
                                                                        (secondaryCol.states.length === 0) ? (
                                                                        <div className='lock-background'>
                                                                        </div>
                                                                    ) : (
                                                                        (secondaryCol.states.length === 1) ? (
                                                                            <div className={`A${parseInt(secondaryCol.states[0].color)}-background`}>
                                                                                <p>{parseInt(secondaryCol.states[0].color)}</p>
                                                                            </div>
                                                                        ) : (
                                                                            <div className='multi-color-background'>
                                                                                <p>M</p>
                                                                            </div>
                                                                        )
                                                                    )
                                                                )}
                                                            </div>
                                                        )}
                                                    </div>
                                                )
                                            )}
                                        </div>
                                    )
                                )}
                            </div>
                        </div>
                        <div className="col">
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default GuestMap;
