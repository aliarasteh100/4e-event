import React, { Component } from 'react';

class PageNotFound extends Component {
    componentWillMount() {
        document.body.className = 'red-background-image';
        this.props.changeLanguageVisibility(true);
    }
    render() {
        return (
            <div className="not-found container">
                <div className="row">
                    <div className="col">
                    </div>
                    <div className="not-found-div col-10 col-sm-10 col-md-8 col-lg-6 col-xl-6">
                        <p className="error404">{this.props.message.error404}</p>
                        <p>{this.props.message.pageNotFoundWithExclamationPoint}</p>
                    </div>
                    <div className="col">
                    </div>
                </div>
            </div>
        )
    }
}
export default PageNotFound;