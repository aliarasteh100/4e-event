import React, { Component } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom'
import Header from "./Header"
import Login from "./Login"
import ForgotPassword from "./ForgotPassword"
import ChangePassword from "./ChangePassword"
import Game from "./Game"
import Rules from "./Rules"
import Resana from "./Resana"
import ShoraieSenfi from "./ShoraieSenfi"
import PageNotFound from "./PageNotFound"
import messages from './languages/index'
import axios from "axios";
import API from "./API/API";
import Auth from "./Auth/Auth";

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isAuthenticated: false,
            baseConfigs: {},
            direction: "rtl",
            message: messages.fa.message,
            changeLanguageVisibility: true,
            map: [
                {
                    "id": 3708,
                    "row": 1,
                    "col1": 1,
                    "col2": 1,
                    "states": [],
                    "must_show": false,
                    "buy_obj": null
                },
                {
                    "id": 3709,
                    "row": 1,
                    "col1": 1,
                    "col2": 2,
                    "states": [],
                    "must_show": false,
                    "buy_obj": null
                },
                {
                    "id": 3710,
                    "row": 1,
                    "col1": 1,
                    "col2": 3,
                    "states": [],
                    "must_show": false,
                    "buy_obj": null
                },
                {
                    "id": 3711,
                    "row": 1,
                    "col1": 1,
                    "col2": 4,
                    "states": [],
                    "must_show": false,
                    "buy_obj": null
                },
                {
                    "id": 3712,
                    "row": 1,
                    "col1": 1,
                    "col2": 5,
                    "states": [],
                    "must_show": false,
                    "buy_obj": null
                },
                {
                    "id": 3713,
                    "row": 1,
                    "col1": 2,
                    "col2": 1,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3714,
                    "row": 1,
                    "col1": 2,
                    "col2": 2,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3715,
                    "row": 1,
                    "col1": 2,
                    "col2": 3,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3716,
                    "row": 1,
                    "col1": 2,
                    "col2": 4,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3717,
                    "row": 1,
                    "col1": 2,
                    "col2": 5,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3718,
                    "row": 1,
                    "col1": 3,
                    "col2": 1,
                    "states": [],
                    "must_show": false,
                    "buy_obj": null
                },
                {
                    "id": 3719,
                    "row": 1,
                    "col1": 3,
                    "col2": 2,
                    "states": [],
                    "must_show": false,
                    "buy_obj": null
                },
                {
                    "id": 3720,
                    "row": 1,
                    "col1": 3,
                    "col2": 3,
                    "states": [],
                    "must_show": false,
                    "buy_obj": null
                },
                {
                    "id": 3721,
                    "row": 1,
                    "col1": 3,
                    "col2": 4,
                    "states": [],
                    "must_show": false,
                    "buy_obj": null
                },
                {
                    "id": 3722,
                    "row": 1,
                    "col1": 3,
                    "col2": 5,
                    "states": [],
                    "must_show": false,
                    "buy_obj": null
                },
                {
                    "id": 3723,
                    "row": 2,
                    "col1": 1,
                    "col2": 1,
                    "states": [],
                    "must_show": false,
                    "buy_obj": null
                },
                {
                    "id": 3724,
                    "row": 2,
                    "col1": 1,
                    "col2": 2,
                    "states": [],
                    "must_show": false,
                    "buy_obj": null
                },
                {
                    "id": 3725,
                    "row": 2,
                    "col1": 1,
                    "col2": 3,
                    "states": [],
                    "must_show": false,
                    "buy_obj": null
                },
                {
                    "id": 3726,
                    "row": 2,
                    "col1": 1,
                    "col2": 4,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3727,
                    "row": 2,
                    "col1": 1,
                    "col2": 5,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3728,
                    "row": 2,
                    "col1": 2,
                    "col2": 1,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3729,
                    "row": 2,
                    "col1": 2,
                    "col2": 2,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3730,
                    "row": 2,
                    "col1": 2,
                    "col2": 3,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3731,
                    "row": 2,
                    "col1": 2,
                    "col2": 4,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3732,
                    "row": 2,
                    "col1": 2,
                    "col2": 5,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3733,
                    "row": 2,
                    "col1": 3,
                    "col2": 1,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3734,
                    "row": 2,
                    "col1": 3,
                    "col2": 2,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3735,
                    "row": 2,
                    "col1": 3,
                    "col2": 3,
                    "states": [],
                    "must_show": false,
                    "buy_obj": null
                },
                {
                    "id": 3736,
                    "row": 2,
                    "col1": 3,
                    "col2": 4,
                    "states": [],
                    "must_show": false,
                    "buy_obj": null
                },
                {
                    "id": 3737,
                    "row": 2,
                    "col1": 3,
                    "col2": 5,
                    "states": [],
                    "must_show": false,
                    "buy_obj": null
                },
                {
                    "id": 3738,
                    "row": 3,
                    "col1": 1,
                    "col2": 1,
                    "states": [],
                    "must_show": false,
                    "buy_obj": null
                },
                {
                    "id": 3739,
                    "row": 3,
                    "col1": 1,
                    "col2": 2,
                    "states": [],
                    "must_show": false,
                    "buy_obj": null
                },
                {
                    "id": 3740,
                    "row": 3,
                    "col1": 1,
                    "col2": 3,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3741,
                    "row": 3,
                    "col1": 1,
                    "col2": 4,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3742,
                    "row": 3,
                    "col1": 1,
                    "col2": 5,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3743,
                    "row": 3,
                    "col1": 2,
                    "col2": 1,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3744,
                    "row": 3,
                    "col1": 2,
                    "col2": 2,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3745,
                    "row": 3,
                    "col1": 2,
                    "col2": 3,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3746,
                    "row": 3,
                    "col1": 2,
                    "col2": 4,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3747,
                    "row": 3,
                    "col1": 2,
                    "col2": 5,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3748,
                    "row": 3,
                    "col1": 3,
                    "col2": 1,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3749,
                    "row": 3,
                    "col1": 3,
                    "col2": 2,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3750,
                    "row": 3,
                    "col1": 3,
                    "col2": 3,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3751,
                    "row": 3,
                    "col1": 3,
                    "col2": 4,
                    "states": [],
                    "must_show": false,
                    "buy_obj": null
                },
                {
                    "id": 3752,
                    "row": 3,
                    "col1": 3,
                    "col2": 5,
                    "states": [],
                    "must_show": false,
                    "buy_obj": null
                },
                {
                    "id": 3753,
                    "row": 4,
                    "col1": 1,
                    "col2": 1,
                    "states": [],
                    "must_show": false,
                    "buy_obj": null
                },
                {
                    "id": 3754,
                    "row": 4,
                    "col1": 1,
                    "col2": 2,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3755,
                    "row": 4,
                    "col1": 1,
                    "col2": 3,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3756,
                    "row": 4,
                    "col1": 1,
                    "col2": 4,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3757,
                    "row": 4,
                    "col1": 1,
                    "col2": 5,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3758,
                    "row": 4,
                    "col1": 2,
                    "col2": 1,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3759,
                    "row": 4,
                    "col1": 2,
                    "col2": 2,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3760,
                    "row": 4,
                    "col1": 2,
                    "col2": 3,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3761,
                    "row": 4,
                    "col1": 2,
                    "col2": 4,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3762,
                    "row": 4,
                    "col1": 2,
                    "col2": 5,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3763,
                    "row": 4,
                    "col1": 3,
                    "col2": 1,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3764,
                    "row": 4,
                    "col1": 3,
                    "col2": 2,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3765,
                    "row": 4,
                    "col1": 3,
                    "col2": 3,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3766,
                    "row": 4,
                    "col1": 3,
                    "col2": 4,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3767,
                    "row": 4,
                    "col1": 3,
                    "col2": 5,
                    "states": [],
                    "must_show": false,
                    "buy_obj": null
                },
                {
                    "id": 3768,
                    "row": 5,
                    "col1": 1,
                    "col2": 1,
                    "states": [],
                    "must_show": false,
                    "buy_obj": null
                },
                {
                    "id": 3769,
                    "row": 5,
                    "col1": 1,
                    "col2": 2,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3770,
                    "row": 5,
                    "col1": 1,
                    "col2": 3,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3771,
                    "row": 5,
                    "col1": 1,
                    "col2": 4,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3772,
                    "row": 5,
                    "col1": 1,
                    "col2": 5,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3773,
                    "row": 5,
                    "col1": 2,
                    "col2": 1,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3774,
                    "row": 5,
                    "col1": 2,
                    "col2": 2,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3775,
                    "row": 5,
                    "col1": 2,
                    "col2": 3,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3776,
                    "row": 5,
                    "col1": 2,
                    "col2": 4,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3777,
                    "row": 5,
                    "col1": 2,
                    "col2": 5,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3778,
                    "row": 5,
                    "col1": 3,
                    "col2": 1,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3779,
                    "row": 5,
                    "col1": 3,
                    "col2": 2,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3780,
                    "row": 5,
                    "col1": 3,
                    "col2": 3,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3781,
                    "row": 5,
                    "col1": 3,
                    "col2": 4,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3782,
                    "row": 5,
                    "col1": 3,
                    "col2": 5,
                    "states": [],
                    "must_show": false,
                    "buy_obj": null
                },
                {
                    "id": 3783,
                    "row": 6,
                    "col1": 1,
                    "col2": 1,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3784,
                    "row": 6,
                    "col1": 1,
                    "col2": 2,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3785,
                    "row": 6,
                    "col1": 1,
                    "col2": 3,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3786,
                    "row": 6,
                    "col1": 1,
                    "col2": 4,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3787,
                    "row": 6,
                    "col1": 1,
                    "col2": 5,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3788,
                    "row": 6,
                    "col1": 2,
                    "col2": 1,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3789,
                    "row": 6,
                    "col1": 2,
                    "col2": 2,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3790,
                    "row": 6,
                    "col1": 2,
                    "col2": 3,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3791,
                    "row": 6,
                    "col1": 2,
                    "col2": 4,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3792,
                    "row": 6,
                    "col1": 2,
                    "col2": 5,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3793,
                    "row": 6,
                    "col1": 3,
                    "col2": 1,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3794,
                    "row": 6,
                    "col1": 3,
                    "col2": 2,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3795,
                    "row": 6,
                    "col1": 3,
                    "col2": 3,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3796,
                    "row": 6,
                    "col1": 3,
                    "col2": 4,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3797,
                    "row": 6,
                    "col1": 3,
                    "col2": 5,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3798,
                    "row": 7,
                    "col1": 1,
                    "col2": 1,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3799,
                    "row": 7,
                    "col1": 1,
                    "col2": 2,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3800,
                    "row": 7,
                    "col1": 1,
                    "col2": 3,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3801,
                    "row": 7,
                    "col1": 1,
                    "col2": 4,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3802,
                    "row": 7,
                    "col1": 1,
                    "col2": 5,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3803,
                    "row": 7,
                    "col1": 2,
                    "col2": 1,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3804,
                    "row": 7,
                    "col1": 2,
                    "col2": 2,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3805,
                    "row": 7,
                    "col1": 2,
                    "col2": 3,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3806,
                    "row": 7,
                    "col1": 2,
                    "col2": 4,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3807,
                    "row": 7,
                    "col1": 2,
                    "col2": 5,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3808,
                    "row": 7,
                    "col1": 3,
                    "col2": 1,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3809,
                    "row": 7,
                    "col1": 3,
                    "col2": 2,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3810,
                    "row": 7,
                    "col1": 3,
                    "col2": 3,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3811,
                    "row": 7,
                    "col1": 3,
                    "col2": 4,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3812,
                    "row": 7,
                    "col1": 3,
                    "col2": 5,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3813,
                    "row": 8,
                    "col1": 1,
                    "col2": 1,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3814,
                    "row": 8,
                    "col1": 1,
                    "col2": 2,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3815,
                    "row": 8,
                    "col1": 1,
                    "col2": 3,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3816,
                    "row": 8,
                    "col1": 1,
                    "col2": 4,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3817,
                    "row": 8,
                    "col1": 1,
                    "col2": 5,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3818,
                    "row": 8,
                    "col1": 2,
                    "col2": 1,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3819,
                    "row": 8,
                    "col1": 2,
                    "col2": 2,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3820,
                    "row": 8,
                    "col1": 2,
                    "col2": 3,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3821,
                    "row": 8,
                    "col1": 2,
                    "col2": 4,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3822,
                    "row": 8,
                    "col1": 2,
                    "col2": 5,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3823,
                    "row": 8,
                    "col1": 3,
                    "col2": 1,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3824,
                    "row": 8,
                    "col1": 3,
                    "col2": 2,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3825,
                    "row": 8,
                    "col1": 3,
                    "col2": 3,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3826,
                    "row": 8,
                    "col1": 3,
                    "col2": 4,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3827,
                    "row": 8,
                    "col1": 3,
                    "col2": 5,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3828,
                    "row": 9,
                    "col1": 1,
                    "col2": 1,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3829,
                    "row": 9,
                    "col1": 1,
                    "col2": 2,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3830,
                    "row": 9,
                    "col1": 1,
                    "col2": 3,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3831,
                    "row": 9,
                    "col1": 1,
                    "col2": 4,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3832,
                    "row": 9,
                    "col1": 1,
                    "col2": 5,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3833,
                    "row": 9,
                    "col1": 2,
                    "col2": 1,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3834,
                    "row": 9,
                    "col1": 2,
                    "col2": 2,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3835,
                    "row": 9,
                    "col1": 2,
                    "col2": 3,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3836,
                    "row": 9,
                    "col1": 2,
                    "col2": 4,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3837,
                    "row": 9,
                    "col1": 2,
                    "col2": 5,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3838,
                    "row": 9,
                    "col1": 3,
                    "col2": 1,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3839,
                    "row": 9,
                    "col1": 3,
                    "col2": 2,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3840,
                    "row": 9,
                    "col1": 3,
                    "col2": 3,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3841,
                    "row": 9,
                    "col1": 3,
                    "col2": 4,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3842,
                    "row": 9,
                    "col1": 3,
                    "col2": 5,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3843,
                    "row": 10,
                    "col1": 1,
                    "col2": 1,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3844,
                    "row": 10,
                    "col1": 1,
                    "col2": 2,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3845,
                    "row": 10,
                    "col1": 1,
                    "col2": 3,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3846,
                    "row": 10,
                    "col1": 1,
                    "col2": 4,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3847,
                    "row": 10,
                    "col1": 1,
                    "col2": 5,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3848,
                    "row": 10,
                    "col1": 2,
                    "col2": 1,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3849,
                    "row": 10,
                    "col1": 2,
                    "col2": 2,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3850,
                    "row": 10,
                    "col1": 2,
                    "col2": 3,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3851,
                    "row": 10,
                    "col1": 2,
                    "col2": 4,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3852,
                    "row": 10,
                    "col1": 2,
                    "col2": 5,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3853,
                    "row": 10,
                    "col1": 3,
                    "col2": 1,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3854,
                    "row": 10,
                    "col1": 3,
                    "col2": 2,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3855,
                    "row": 10,
                    "col1": 3,
                    "col2": 3,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3856,
                    "row": 10,
                    "col1": 3,
                    "col2": 4,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3857,
                    "row": 10,
                    "col1": 3,
                    "col2": 5,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3858,
                    "row": 11,
                    "col1": 1,
                    "col2": 1,
                    "states": [],
                    "must_show": false,
                    "buy_obj": null
                },
                {
                    "id": 3859,
                    "row": 11,
                    "col1": 1,
                    "col2": 2,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3860,
                    "row": 11,
                    "col1": 1,
                    "col2": 3,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3861,
                    "row": 11,
                    "col1": 1,
                    "col2": 4,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3862,
                    "row": 11,
                    "col1": 1,
                    "col2": 5,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3863,
                    "row": 11,
                    "col1": 2,
                    "col2": 1,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3864,
                    "row": 11,
                    "col1": 2,
                    "col2": 2,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3865,
                    "row": 11,
                    "col1": 2,
                    "col2": 3,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3866,
                    "row": 11,
                    "col1": 2,
                    "col2": 4,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3867,
                    "row": 11,
                    "col1": 2,
                    "col2": 5,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3868,
                    "row": 11,
                    "col1": 3,
                    "col2": 1,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3869,
                    "row": 11,
                    "col1": 3,
                    "col2": 2,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3870,
                    "row": 11,
                    "col1": 3,
                    "col2": 3,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3871,
                    "row": 11,
                    "col1": 3,
                    "col2": 4,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3872,
                    "row": 11,
                    "col1": 3,
                    "col2": 5,
                    "states": [],
                    "must_show": false,
                    "buy_obj": null
                },
                {
                    "id": 3873,
                    "row": 12,
                    "col1": 1,
                    "col2": 1,
                    "states": [],
                    "must_show": false,
                    "buy_obj": null
                },
                {
                    "id": 3874,
                    "row": 12,
                    "col1": 1,
                    "col2": 2,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3875,
                    "row": 12,
                    "col1": 1,
                    "col2": 3,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3876,
                    "row": 12,
                    "col1": 1,
                    "col2": 4,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3877,
                    "row": 12,
                    "col1": 1,
                    "col2": 5,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3878,
                    "row": 12,
                    "col1": 2,
                    "col2": 1,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3879,
                    "row": 12,
                    "col1": 2,
                    "col2": 2,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3880,
                    "row": 12,
                    "col1": 2,
                    "col2": 3,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3881,
                    "row": 12,
                    "col1": 2,
                    "col2": 4,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3882,
                    "row": 12,
                    "col1": 2,
                    "col2": 5,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3883,
                    "row": 12,
                    "col1": 3,
                    "col2": 1,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3884,
                    "row": 12,
                    "col1": 3,
                    "col2": 2,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3885,
                    "row": 12,
                    "col1": 3,
                    "col2": 3,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3886,
                    "row": 12,
                    "col1": 3,
                    "col2": 4,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3887,
                    "row": 12,
                    "col1": 3,
                    "col2": 5,
                    "states": [],
                    "must_show": false,
                    "buy_obj": null
                },
                {
                    "id": 3888,
                    "row": 13,
                    "col1": 1,
                    "col2": 1,
                    "states": [],
                    "must_show": false,
                    "buy_obj": null
                },
                {
                    "id": 3889,
                    "row": 13,
                    "col1": 1,
                    "col2": 2,
                    "states": [],
                    "must_show": false,
                    "buy_obj": null
                },
                {
                    "id": 3890,
                    "row": 13,
                    "col1": 1,
                    "col2": 3,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3891,
                    "row": 13,
                    "col1": 1,
                    "col2": 4,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3892,
                    "row": 13,
                    "col1": 1,
                    "col2": 5,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3893,
                    "row": 13,
                    "col1": 2,
                    "col2": 1,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3894,
                    "row": 13,
                    "col1": 2,
                    "col2": 2,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3895,
                    "row": 13,
                    "col1": 2,
                    "col2": 3,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3896,
                    "row": 13,
                    "col1": 2,
                    "col2": 4,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3897,
                    "row": 13,
                    "col1": 2,
                    "col2": 5,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3898,
                    "row": 13,
                    "col1": 3,
                    "col2": 1,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3899,
                    "row": 13,
                    "col1": 3,
                    "col2": 2,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3900,
                    "row": 13,
                    "col1": 3,
                    "col2": 3,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3901,
                    "row": 13,
                    "col1": 3,
                    "col2": 4,
                    "states": [],
                    "must_show": false,
                    "buy_obj": null
                },
                {
                    "id": 3902,
                    "row": 13,
                    "col1": 3,
                    "col2": 5,
                    "states": [],
                    "must_show": false,
                    "buy_obj": null
                },
                {
                    "id": 3903,
                    "row": 14,
                    "col1": 1,
                    "col2": 1,
                    "states": [],
                    "must_show": false,
                    "buy_obj": null
                },
                {
                    "id": 3904,
                    "row": 14,
                    "col1": 1,
                    "col2": 2,
                    "states": [],
                    "must_show": false,
                    "buy_obj": null
                },
                {
                    "id": 3905,
                    "row": 14,
                    "col1": 1,
                    "col2": 3,
                    "states": [],
                    "must_show": false,
                    "buy_obj": null
                },
                {
                    "id": 3906,
                    "row": 14,
                    "col1": 1,
                    "col2": 4,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3907,
                    "row": 14,
                    "col1": 1,
                    "col2": 5,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3908,
                    "row": 14,
                    "col1": 2,
                    "col2": 1,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3909,
                    "row": 14,
                    "col1": 2,
                    "col2": 2,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3910,
                    "row": 14,
                    "col1": 2,
                    "col2": 3,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3911,
                    "row": 14,
                    "col1": 2,
                    "col2": 4,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3912,
                    "row": 14,
                    "col1": 2,
                    "col2": 5,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3913,
                    "row": 14,
                    "col1": 3,
                    "col2": 1,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3914,
                    "row": 14,
                    "col1": 3,
                    "col2": 2,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3915,
                    "row": 14,
                    "col1": 3,
                    "col2": 3,
                    "states": [],
                    "must_show": false,
                    "buy_obj": null
                },
                {
                    "id": 3916,
                    "row": 14,
                    "col1": 3,
                    "col2": 4,
                    "states": [],
                    "must_show": false,
                    "buy_obj": null
                },
                {
                    "id": 3917,
                    "row": 14,
                    "col1": 3,
                    "col2": 5,
                    "states": [],
                    "must_show": false,
                    "buy_obj": null
                },
                {
                    "id": 3918,
                    "row": 15,
                    "col1": 1,
                    "col2": 1,
                    "states": [],
                    "must_show": false,
                    "buy_obj": null
                },
                {
                    "id": 3919,
                    "row": 15,
                    "col1": 1,
                    "col2": 2,
                    "states": [],
                    "must_show": false,
                    "buy_obj": null
                },
                {
                    "id": 3920,
                    "row": 15,
                    "col1": 1,
                    "col2": 3,
                    "states": [],
                    "must_show": false,
                    "buy_obj": null
                },
                {
                    "id": 3921,
                    "row": 15,
                    "col1": 1,
                    "col2": 4,
                    "states": [],
                    "must_show": false,
                    "buy_obj": null
                },
                {
                    "id": 3922,
                    "row": 15,
                    "col1": 1,
                    "col2": 5,
                    "states": [],
                    "must_show": false,
                    "buy_obj": null
                },
                {
                    "id": 3923,
                    "row": 15,
                    "col1": 2,
                    "col2": 1,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3924,
                    "row": 15,
                    "col1": 2,
                    "col2": 2,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3925,
                    "row": 15,
                    "col1": 2,
                    "col2": 3,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3926,
                    "row": 15,
                    "col1": 2,
                    "col2": 4,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3927,
                    "row": 15,
                    "col1": 2,
                    "col2": 5,
                    "states": [],
                    "must_show": true,
                    "buy_obj": null
                },
                {
                    "id": 3928,
                    "row": 15,
                    "col1": 3,
                    "col2": 1,
                    "states": [],
                    "must_show": false,
                    "buy_obj": null
                },
                {
                    "id": 3929,
                    "row": 15,
                    "col1": 3,
                    "col2": 2,
                    "states": [],
                    "must_show": false,
                    "buy_obj": null
                },
                {
                    "id": 3930,
                    "row": 15,
                    "col1": 3,
                    "col2": 3,
                    "states": [],
                    "must_show": false,
                    "buy_obj": null
                },
                {
                    "id": 3931,
                    "row": 15,
                    "col1": 3,
                    "col2": 4,
                    "states": [],
                    "must_show": false,
                    "buy_obj": null
                },
                {
                    "id": 3932,
                    "row": 15,
                    "col1": 3,
                    "col2": 5,
                    "states": [],
                    "must_show": false,
                    "buy_obj": null
                }
            ],
            changedMap: [],
            pointToMoney: null,
            moneyToPoint: null,
            ranks: [],
            messages: []
        };
    }
    componentWillMount() {
        this.changeMapData();
    }
    componentDidMount() {
        this.isAuthenticatedInterval = setInterval(() => this.isAuthenticated(), 1000);
        this.getActiveMapInterval = setInterval(() => {
            if (this.state.isAuthenticated) {
                this.getActiveMap();
            }
        }, 5000);
        this.getPriceInterval = setInterval(() => {
            if (this.state.isAuthenticated) {
                this.getPrice();
            }
        }, 300000);
        this.rankingInterval = setInterval(() => {
            if (this.state.isAuthenticated) {
                this.ranking();
            }
        }, 15000);
        this.messagesInterval = setInterval(() => {
            if (this.state.isAuthenticated) {
                this.messages();
            }
        }, 60000);
    }
    componentWillUnmount() {
        clearInterval(this.isAuthenticatedInterval);
        clearInterval(this.getActiveMapInterval);
        clearInterval(this.getPriceInterval);
        clearInterval(this.rankingInterval);
        clearInterval(this.messagesInterval);
    }
    changeLanguageVisibility = (isVisible) => {
        this.setState({
            changeLanguageVisibility: isVisible,
        });
    };
    changeLanguage = (language) => {
        if (language === 'change') {
            this.setState({
                direction: (this.state.direction === 'rtl') ? 'ltr' : 'rtl',
                message: (this.state.direction === 'rtl') ? messages.en.message : messages.fa.message
            });
        }
        else if (language === 'fa') {
            this.setState({
                direction: 'rtl',
                message: messages.fa.message
            })
        }
    };
    isAuthenticated = () => {
        if (localStorage.getItem('access_token') && (new Date().getTime()/1000 <= localStorage.getItem('expires_in'))) {
            if (!this.state.isAuthenticated) {
                this.getBaseConfigs();
                this.getActiveMap();
                this.getPrice();
                this.ranking();
                this.messages();
            }
            this.setState({
                isAuthenticated: true
            });
        }
        else {
            localStorage.removeItem('refresh_token');
            localStorage.removeItem('token_type');
            localStorage.removeItem('access_token');
            localStorage.removeItem('expires_in');
            this.setState({
                isAuthenticated: false
            });
        }
    };
    refreshToken = () => {
        axios({
            url: `${API.baseRefreshTokenURL}/o/auth/token/`,
            method: "post",
            data: {
                lang: '',
                client_id: Auth.client_id,
                client_secret: Auth.client_secret,
                grant_type: "refresh_token",
                refresh_token: `${localStorage.getItem("refresh_token")}`
            },
            headers: {
                "content-type": "application/json",
                "Access-Control-Allow-Origin": "*"
            },
            crossDomain: true,
        }).then(response => {
            localStorage.setItem('refresh_token', response.data.refresh_token);
            localStorage.setItem('token_type', response.data.token_type);
            localStorage.setItem('access_token', response.data.access_token);
            localStorage.setItem('expires_in', new Date().getTime()/1000 + response.data.expires_in);
            this.isAuthenticated();
            console.log("hello");
        }).catch(err => {
            localStorage.removeItem('refresh_token');
            localStorage.removeItem('token_type');
            localStorage.removeItem('access_token');
            localStorage.removeItem('expires_in');
            this.isAuthenticated();
            console.log(err.message);
        });
    };
    getBaseConfigs = () => {
        axios({
            url: `${API.baseURL}/configs/?lang=${(this.props.direction === 'rtl') ? 'fa' : 'en'}`,
            method: "get",
            headers: {
                "content-type": "application/json",
                "Access-Control-Allow-Origin": "*",
                "Authorization": `${localStorage.getItem("token_type")} ${localStorage.getItem("access_token")}`
            },
            crossDomain: true,
        }).then(response => {
            this.setState({
                baseConfigs: response.data
            });
        }).catch(err => {
            console.log(err.message);
        });
    };
    getActiveMap = () => {
        axios({
            url: `${API.baseURL}/get-active-map/?lang=en`,
            method: "get",
            headers: {
                "content-type": "application/json",
                "Access-Control-Allow-Origin": "*",
                "Authorization": `${localStorage.getItem("token_type")} ${localStorage.getItem("access_token")}`
            },
            crossDomain: true,
        }).then(response => {
            this.setState({
                map: response.data
            });
            this.changeMapData();
        }).catch(err => {
            console.log(err.message);
        });
    };
    changeMapData = () => {
        let i = 1;
        let arrayCol2 = [];
        let mapCol2 = [];
        this.state.map.forEach((node) => {
            arrayCol2.push(node);
            if (node.col2 % 5 === 0) {
                mapCol2.push({
                    id: i.toString() + i.toString(),
                    col1: node.col1,
                    row: node.row,
                    colElements: arrayCol2
                });
                i = i + 1;
                arrayCol2 = [];
            }
        });
        let j = 1;
        let arrayCol1 = [];
        let mapCol1 = [];
        mapCol2.forEach((col2) => {
            arrayCol1.push(col2);
            if (col2.col1 % 3 === 0) {
                mapCol1.push({
                    id: j.toString(),
                    row: col2.row,
                    rowElements: arrayCol1
                });
                j = j + 1;
                arrayCol1 = [];
            }
        });
        this.setState({
            changedMap: mapCol1
        })
    };
    getPrice = () => {
        axios({
            url: `${API.baseURL}/exchange/`,
            method: "get",
            headers: {
                "content-type": "application/json",
                "Access-Control-Allow-Origin": "*",
                "Authorization": `${localStorage.getItem("token_type")} ${localStorage.getItem("access_token")}`
            },
            crossDomain: true,
        }).then(response => {
            this.setState({
                pointToMoney: parseFloat(response.data.rate),
                moneyToPoint: 1.025 * parseFloat(response.data.rate)
            })
        }).catch(err => {
            console.log(err.message);
        });
    };
    ranking = () => {
        axios({
            url: `${API.baseURL}/ranks/`,
            method: "get",
            headers: {
                "content-type": "application/json",
                "Access-Control-Allow-Origin": "*",
                "Authorization": `${localStorage.getItem("token_type")} ${localStorage.getItem("access_token")}`
            },
            crossDomain: true,
        }).then(response => {
            this.setState({
                ranks: response.data
            });
        }).catch(err => {
            console.log(err.message);
        });
    };
    messages = () => {
        axios({
            url: `${API.baseURL}/client/messages/?lang=${(this.state.direction === 'rtl') ? 'fa' : 'en'}`,
            method: "get",
            headers: {
                "content-type": "application/json",
                "Access-Control-Allow-Origin": "*",
                "Authorization": `${localStorage.getItem("token_type")} ${localStorage.getItem("access_token")}`
            },
            crossDomain: true,
        }).then(response => {
            this.setState({
                messages: response.data
            });
        }).catch(err => {
            console.log(err.message);
        });
    };
    render() {
        return (
            <div style={{direction: this.state.direction}}>
                <Header isAuthenticated={this.state.isAuthenticated} isAuthenticatedFunction={this.isAuthenticated} direction={this.state.direction} message={this.state.message} changeLanguage={this.changeLanguage} changeLanguageVisibility={this.state.changeLanguageVisibility}/>
                <Switch>
                    {/*<Route exact path="/" render={props => <Home {...props} direction={this.state.direction} message={this.state.message} changeLanguageVisibility={this.changeLanguageVisibility}/>}/>*/}
                    <Route exact path="/" render={props => <Redirect {...props} to="/login"/>}/>
                    <Route exact path="/login/" render={props => (this.state.isAuthenticated) ? <Redirect to='/game'/> : <Login {...props} direction={this.state.direction} message={this.state.message} changeLanguageVisibility={this.changeLanguageVisibility} isAuthenticated={this.isAuthenticated}/>}/>
                    <Route exact path="/forgot-password/" render={props => (this.state.isAuthenticated) ? <Redirect to='/game'/> : <ForgotPassword {...props} direction={this.state.direction} message={this.state.message} changeLanguageVisibility={this.changeLanguageVisibility}/>}/>
                    <Route exact path="/change-password/:forgot_id/" render={props => (this.state.isAuthenticated) ? <Redirect to='/game'/> : <ChangePassword {...props} direction={this.state.direction} message={this.state.message} changeLanguageVisibility={this.changeLanguageVisibility}/>}/>
                    <Route exact path="/game" render={props => (this.state.isAuthenticated) ? <Game {...props} direction={this.state.direction} message={this.state.message} changeLanguageVisibility={this.changeLanguageVisibility} changeLanguage={this.changeLanguage} map={this.state.changedMap} pointToMoney={this.state.pointToMoney} moneyToPoint={this.state.moneyToPoint} ranks={this.state.ranks} rankingType={this.state.rankingType} changeRankingBase={this.changeRankingBase} messages={this.state.messages} messagesFunction={this.messages}/> : <Redirect to='/login'/>}/>
                    <Route exact path="/Rules/" render={props => <Rules {...props} direction={this.state.direction} message={this.state.message} changeLanguageVisibility={this.changeLanguageVisibility}/>}/>
                    <Route exact path="/Resana/" render={props => <Resana {...props} direction={this.state.direction} message={this.state.message} changeLanguageVisibility={this.changeLanguageVisibility}/>}/>
                    <Route exact path="/Shoraie-Senfi/" render={props => <ShoraieSenfi {...props} direction={this.state.direction} message={this.state.message} changeLanguageVisibility={this.changeLanguageVisibility}/>}/>
                    <Route exact path="/page-not-found/" render={props => <PageNotFound {...props} direction={this.state.direction} message={this.state.message} changeLanguageVisibility={this.changeLanguageVisibility}/>}/>
                    <Route path="/*" render={props => <Redirect {...props} to="/page-not-found"/>}/>
                </Switch>
            </div>
        )
    }
}
export default App;
