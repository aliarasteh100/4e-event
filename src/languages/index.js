import en from './En/index';
import fa from './Fa/index';

export default {
    en: {
        message: en
    },
    fa: {
        message: fa
    }
}