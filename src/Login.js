import React, { Component } from 'react';
import { FaEnvelope } from "react-icons/fa";
import { FaUser } from "react-icons/fa";
import { FaLock } from "react-icons/fa";
import { FaLongArrowAltLeft } from "react-icons/fa"
import { FaLongArrowAltRight } from "react-icons/fa"
import { MdVisibility } from "react-icons/md";
import { MdVisibilityOff } from "react-icons/md";
import { Link } from "react-router-dom";
import Auth from "./Auth/Auth";
import API from "./API/API";
import axios from 'axios';

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isVisible: false,
            loginOrSignUp: 'login',
            errorMessageVisibility: false,
            loading: false
        }
    }
    componentWillMount() {
        document.body.className = 'cyan-background-image';
        this.props.changeLanguageVisibility(true);
    }
    changeVisibility = () => {
        this.setState({
            isVisible: !this.state.isVisible
        });
    };
    signUp = () => {
        let email = this.refs.email.value;
        let username = this.refs.username.value;
        let password = this.refs.password.value;
        this.setState({
            loading: true
        });
        axios({
            url: `${API.baseURL}/register/`,
            method: "post",
            data: {
                lang: (this.props.direction === 'rtl') ? 'fa' : 'en',
                client_id: Auth.client_id,
                client_secret: Auth.client_secret,
                grant_type: "password",
                email: email,
                username: username,
                password: password
            },
            headers: {
                "content-type": "application/json",
                "Access-Control-Allow-Origin": "*"
            },
            crossDomain: true,
        }).then(response => {
            localStorage.setItem('is_guest', response.data.is_guest);
            localStorage.setItem('refresh_token', response.data.refresh_token);
            localStorage.setItem('token_type', response.data.token_type);
            localStorage.setItem('access_token', response.data.access_token);
            localStorage.setItem('expires_in', new Date().getTime()/1000 + response.data.expires_in);
            this.props.isAuthenticated();
            this.setState({
                loading: false
            });
        }).catch(err => {
            if (err.response) {
                if (err.response.data) {
                    if (err.response.data.error) {
                        this.displayError(err.response.data.error)
                    }
                }
            } else {
                this.displayError(err.message)
            }
            this.setState({
                loading: false
            });
        });
    };
    login = () => {
        let username = this.refs.username.value;
        let password = this.refs.password.value;
        this.setState({
            loading: true
        });
        axios({
            url: `${API.baseURL}/login/`,
            method: "post",
            data: {
                lang: (this.props.direction === 'rtl') ? 'fa' : 'en',
                grant_type: "password",
                client_id: Auth.client_id,
                client_secret: Auth.client_secret,
                username: username,
                password: password
            },
            headers: {
                "content-type": "application/json",
                "Access-Control-Allow-Origin": "*"
            },
            crossDomain: true,
        }).then(response => {
            localStorage.setItem('is_guest', response.data.is_guest);
            localStorage.setItem('refresh_token', response.data.refresh_token);
            localStorage.setItem('token_type', response.data.token_type);
            localStorage.setItem('access_token', response.data.access_token);
            localStorage.setItem('expires_in', new Date().getTime()/1000 + response.data.expires_in);
            this.props.isAuthenticated();
            this.setState({
                loading: false
            });
        }).catch(err => {
            if (err.response) {
                if (err.response.data) {
                    if (err.response.data.error) {
                        this.displayError(err.response.data.error)
                    }
                }
            } else {
                this.displayError(err.message)
            }
            this.setState({
                loading: false
            });
        });
    };
    displayError = (error) => {
        this.refs.errorMessage.innerHTML = error;
        this.setState({
            errorMessageVisibility: true
        })
    };
    changeLoginOrSignUp = (loginOrSignUp) => {
        if (this.state.loginOrSignUp === 'login') {
            this.refs.username.value = '';
            this.refs.password.value = '';
        }
        else if (this.state.loginOrSignUp === 'signUp') {
            this.refs.email.value = '';
            this.refs.username.value = '';
            this.refs.password.value = '';
        }
        this.setState({
            loginOrSignUp: loginOrSignUp
        });
    };
    render() {
        return (
            <div className="login container">
                <div className="row">
                    <div className="col">
                    </div>
                    <div className="user col-10 col-sm-10 col-md-8 col-lg-6 col-xl-6">
                        <div className="row no-gutters">
                            <div className="col">
                            </div>
                            <div className="col-5 col-sm-5 col-md-4 col-lg-3 col-xl-3">
                                <FaUser className="user-icon"/>
                            </div>
                            <div className="col">
                            </div>
                        </div>
                        <div className="container">
                            <div className="row">
                                <div className="login-sign-up col" onClick={() => this.changeLoginOrSignUp('login')} style={{opacity: (this.state.loginOrSignUp === 'login') ? '1' : '0.75'}}>
                                    <p>{this.props.message.login}</p>
                                </div>
                                <div className="login-sign-up col" onClick={() => this.changeLoginOrSignUp('signUp')} style={{opacity: (this.state.loginOrSignUp === 'signUp') ? '1' : '0.75'}}>
                                    <p>{this.props.message.signUp}</p>
                                </div>
                            </div>
                        </div>
                        <div className="login-form">
                            <div className={`error-message ${(this.state.errorMessageVisibility) ? '' : 'd-none'}`} style={{textAlign: ((this.props.direction === 'rtl') ? 'right' : 'left'), marginTop: '20px'}}>
                                <p ref='errorMessage'>
                                </p>
                            </div>
                            {(this.state.loginOrSignUp === 'signUp') ? (
                                <div>
                                    <label htmlFor='Email' style={{textAlign: ((this.props.direction === 'rtl') ? 'right' : 'left'), [(this.props.direction === 'rtl') ? 'marginRight' : 'marginLeft']: '10%'}}><FaEnvelope/>&nbsp;&nbsp;&nbsp;{this.props.message.emailWithColon}</label>
                                    <input id='Email' type='email' maxLength="50" onFocus={() => {this.setState({errorMessageVisibility: false})}} placeholder={this.props.message.email} ref='email' style={{textAlign: (this.props.direction === 'rtl') ? 'right' : 'left', [(this.props.direction === 'rtl') ? 'marginRight' : 'marginLeft']: '10%', [(this.props.direction === 'rtl') ? 'paddingLeft' : 'paddingRight']: '5.5'}} />
                                </div>
                            ) : ''}
                            <label htmlFor='Username' style={{textAlign: ((this.props.direction === 'rtl') ? 'right' : 'left'), [(this.props.direction === 'rtl') ? 'marginRight' : 'marginLeft']: '10%'}}><FaUser/>&nbsp;&nbsp;&nbsp;{this.props.message.usernameWithColon}</label>
                            <input id='Username' type='text' maxLength="50" onFocus={() => {this.setState({errorMessageVisibility: false})}} placeholder={this.props.message.username} ref='username' style={{textAlign: (this.props.direction === 'rtl') ? 'right' : 'left', [(this.props.direction === 'rtl') ? 'marginRight' : 'marginLeft']: '10%', [(this.props.direction === 'rtl') ? 'paddingLeft' : 'paddingRight']: '5.5%'}} />
                            <div className={`error-message ${(this.state.usernameErrorMessageVisibility) ? '' : 'd-none'}`}>
                                <p>{this.props.message.checkUsernameCharactersMessage}</p>
                            </div>
                            <label htmlFor='Password' style={{textAlign: ((this.props.direction === 'rtl') ? 'right' : 'left'), [(this.props.direction === 'rtl') ? 'marginRight' : 'marginLeft']: '10%'}}><FaLock/>&nbsp;&nbsp;&nbsp;{this.props.message.passwordWithColon}</label>
                            <input id='Password' type={(this.state.isVisible) ? 'text' : 'password'} maxLength="50" onFocus={() => {this.setState({errorMessageVisibility: false})}} placeholder={this.props.message.password} ref='password' style={{textAlign: (this.props.direction === 'rtl') ? 'right' : 'left', [(this.props.direction === 'rtl') ? 'marginRight' : 'marginLeft']: '10%', [(this.props.direction === 'rtl') ? 'paddingLeft' : 'paddingRight']: '15%'}} />
                            {(this.state.isVisible) ? <MdVisibility className="visibility-icon" style={{[(this.props.direction === 'rtl') ? 'right' : 'left']: '80.5%'}} onClick={this.changeVisibility}/> : <MdVisibilityOff className="visibility-icon" style={{[(this.props.direction === 'rtl') ? 'right' : 'left']: '80.5%'}} onClick={this.changeVisibility}/>}
                            {(this.state.loginOrSignUp === 'login') ? (
                                <div className="forgot-password" style={{textAlign: ((this.props.direction === 'rtl') ? 'right' : 'left')}}>
                                    <Link to="/forgot-password" style={{[(this.props.direction === 'rtl') ? 'marginRight' : 'marginLeft']: '15.5%'}}>{this.props.message.forgotPasswordWithQuestionMark}</Link>
                                </div>
                            ) : ''}
                            <div style={{marginTop: (this.state.loginOrSignUp === 'login') ? '0' : '-30px'}}>
                                <button onClick={(this.state.loginOrSignUp === 'login') ? this.login : this.signUp}>{(this.state.loginOrSignUp === 'login') ? this.props.message.login : this.props.message.signUp}&nbsp;&nbsp;&nbsp;{(this.props.direction === 'rtl') ? <FaLongArrowAltLeft style={{animation: (this.state.loading) ? '0.75s loading ease-in-out infinite' : 'none' }} className="login-icon"/> : <FaLongArrowAltRight style={{animation: (this.state.loading) ? '0.75s loading ease-in-out infinite' : 'none' }} className="login-icon"/>}</button>
                            </div>
                        </div>
                    </div>
                    <div className="col">
                    </div>
                </div>
            </div>
        )
    }
}
export default Login;