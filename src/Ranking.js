import React, { Component } from 'react';
import { FaArrowUp } from "react-icons/fa";

class Ranking extends Component {
    constructor(props) {
        super(props);
        this.state = {
            rankingType: 'point'
        }
    }
    changeRankingBase = () => {
        (this.state.rankingType === 'point') ? this.setState({
            rankingType: 'money'
        }) : this.setState({
            rankingType: 'point'
        });
    };
    render() {
        return (
            <div className="single-content-page container" style={{textAlign: (this.props.direction === 'rtl') ? 'right' : 'left'}}>
                <div>
                    <div className="header" style={{[(this.props.direction === 'rtl') ? 'right' : 'left']: '30%'}}>{this.props.message.ranking}</div>
                    {
                        (this.props.ranks.length === 0) ? (
                            ''
                        ) : (
                            <table>
                                <tbody>
                                <tr>
                                    <th>{this.props.message.rank}</th>
                                    <th>{this.props.message.groupName}</th>
                                    <th onClick={this.changeRankingBase}>{this.props.message.point}{(this.state.rankingType === 'point') ? <FaArrowUp/> : ''}</th>
                                    <th onClick={this.changeRankingBase}>{this.props.message.money}{(this.state.rankingType === 'money') ? <FaArrowUp/> : ''}</th>
                                </tr>
                                {(this.state.rankingType === 'point') ? this.props.ranks.points.map((group,index) => (
                                        <tr key={index}>
                                            <td>{index + 1}</td>
                                            <td>{group.fa_name}</td>
                                            <td>{group.point}</td>
                                            <td>{group.money}</td>
                                        </tr>
                                    )
                                ) : this.props.ranks.money.map((group,index) => (
                                        <tr key={index}>
                                            <td>{index + 1}</td>
                                            <td>{group.fa_name}</td>
                                            <td>{group.point}</td>
                                            <td>{group.money}</td>
                                        </tr>
                                    )
                                )}
                                </tbody>
                            </table>
                        )
                    }
                </div>
            </div>
        )
    }
}
export default Ranking;