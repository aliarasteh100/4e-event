import React, { Component } from 'react';
import { FaLongArrowAltRight } from "react-icons/fa";
import { FaLongArrowAltLeft } from "react-icons/fa";
import axios from "axios";
import API from "./API/API";
import Auth from "./Auth/Auth";

class Exchange extends Component {
    constructor(props) {
        super(props);
        this.state = {
            type: 'point',
            errorMessageVisibility: false
        }
    }
    exchange = () => {
        let value = parseInt(this.refs.point.value);
        axios({
            url: `${API.baseURL}/exchange/`,
            method: "post",
            data: {
                lang: (this.props.direction === 'rtl') ? 'fa' : 'en',
                client_id: Auth.client_id,
                client_secret: Auth.client_secret,
                grant_type: "password",
                type: this.state.type,
                point: value
            },
            headers: {
                "content-type": "application/json",
                "Access-Control-Allow-Origin": "*",
                "Authorization": `${localStorage.getItem("token_type")} ${localStorage.getItem("access_token")}`
            },
            crossDomain: true,
        }).then(() => {
            this.displayError(this.props.message.successfulExchange)
        }).catch(err => {
            this.displayError(err.message);
        });
    };
    displayError = (error) => {
        this.refs.errorMessage.innerHTML = error;
        this.setState({
            errorMessageVisibility: true
        })
    };
    changeType = (type) => {
        this.setState({
            type: type
        });
        let value = parseInt(this.refs.point.value);
        this.refs.money.value = (this.state.type === type) ? value * this.props.pointToMoney : value * this.props.moneyToPoint;
    };
    changeInput = () => {
        let value = parseInt(this.refs.point.value);
        if (isNaN(value)) {
            value = 0;
        }
        if (value > 100) {
            value = 100;
        }
        this.refs.point.value = value;
        this.refs.money.value = (this.state.type === 'point') ? value * this.props.pointToMoney : value * this.props.moneyToPoint;
    };
    render() {
        return (
            <div className="single-content-page container-fluid" style={{textAlign: (this.props.direction === 'rtl') ? 'right' : 'left'}}>
                <div>
                    <div className="header" style={{[(this.props.direction === 'rtl') ? 'right' : 'left']: '30%'}}>{this.props.message.exchange}</div>
                    <div className="exchange container">
                        <div className="row no-gutters">
                            <div className="col">

                            </div>
                            <div className="col-lg-6 col-sm-8 col-10">
                                <div className="row no-gutters">
                                    <div className="col">
                                        <div className="credit">
                                            <div className="credit-yellow" onClick={() => this.changeType('point')} style={{backgroundColor: (this.state.type === 'point') ? 'yellow' : 'white'}}>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-4">
                                        <p>تبدیل امتیاز به پول</p>
                                    </div>
                                    <div className="col">
                                        <div className="credit">
                                            <div className="credit-yellow" onClick={() => this.changeType('money')} style={{backgroundColor: (this.state.type === 'money') ? 'yellow' : 'white'}}>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-4">
                                        <p>تبدیل پول به امتیاز</p>
                                    </div>
                                </div>
                            </div>
                            <div className="col">

                            </div>
                        </div>
                        <div className="row no-gutters">
                            <div className="col">

                            </div>
                            <div className="col-lg-6 col-sm-8 col-10">
                                <div className="row no-gutters">
                                    <div className="col">

                                    </div>
                                    <div className="col-2">
                                        <p>امتیاز</p>
                                    </div>
                                    <div className="col-2">
                                        <input type="text" ref='point' onChange={this.changeInput}/>
                                    </div>
                                    <div className="col-2">
                                        {(this.state.type === "point") ? <FaLongArrowAltLeft/> : <FaLongArrowAltRight/>}
                                    </div>
                                    <div className="col-2">
                                        <input type="text" ref='money' disabled/>
                                    </div>
                                    <div className="col-2">
                                        <p>پول</p>
                                    </div>
                                    <div className="col">

                                    </div>
                                </div>
                            </div>
                            <div className="col">

                            </div>
                        </div>
                        <div className="row">
                            <div className="col">

                            </div>
                            <div className="col">
                                <p>نرخ تبدیل:</p>
                            </div>
                            <div className="col">

                            </div>
                        </div>
                        <div className="row no-gutters">
                            <div className="col">

                            </div>
                            <div className="col-lg-6 col-sm-8 col-10">
                                <div className="row no-gutters">
                                    <div className="col">

                                    </div>
                                    <div className="col-2">
                                        <p>امتیاز</p>
                                    </div>
                                    <div className="col-2">
                                        <input type="text" value='1' disabled/>
                                    </div>
                                    <div className="col-2">
                                        {(this.state.type === "point") ? <FaLongArrowAltLeft/> : <FaLongArrowAltRight/>}
                                    </div>
                                    <div className="col-2">
                                        <input type="text" value={(this.state.type === "point") ? this.props.pointToMoney : this.props.moneyToPoint} disabled/>
                                    </div>
                                    <div className="col-2">
                                        <p>پول</p>
                                    </div>
                                    <div className="col">

                                    </div>
                                </div>
                            </div>
                            <div className="col">

                            </div>
                        </div>
                        <div className="row">
                            <div className="col">

                            </div>
                            <div className="col">
                                <button onClick={this.exchange}>مبادله</button>
                            </div>
                            <div className="col">

                            </div>
                        </div>
                        <div className={`error-message ${(this.state.errorMessageVisibility) ? '' : 'd-none'}`} style={{textAlign: ((this.props.direction === 'rtl') ? 'right' : 'left'), marginTop: '20px'}}>
                            <p ref='errorMessage'>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default Exchange;