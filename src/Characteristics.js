import React, { Component } from 'react';

class Characteristics extends Component {
    render() {
        return (
            <div className="single-content-page container" style={{textAlign: (this.props.direction === 'rtl') ? 'right' : 'left'}}>
                <div>
                    <div className="header" style={{[(this.props.direction === 'rtl') ? 'right' : 'left']: '30%'}}>{this.props.message.characteristics}</div>
                </div>
            </div>
        )
    }
}
export default Characteristics;